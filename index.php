<?php require_once './code.php';?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Activity</title>
</head>
<body>
	<h3>2. In code.php, create a function named printDivisibleOfFive that will perform the following:
    - Using loops, print all numbers that are divisible by 5 from 0 to 1000.
    - Stop the loop when the loop reaches its 100th iteration.
    - Invoke the function in the index.php</h3>

    <pre><?php modifiedForLoop();?></pre>

    <h3>3. In code.php again, create an empty array named "students".</h3>

    <pre><?php print_r($studentsArray);?></pre>
    <h4>  - Accept a name of the student and add it to the "students" array.</h4>

    <pre><?php array_push($studentsArray, 'John Smith'); ?></pre>
    <pre><?php print_r($studentsArray);?></pre>
    <pre><?php echo count($studentsArray);?></pre>
    <h4>  - Print the names added so far in the "students" array.<br>  - Count the number of names in the "students" array. <br>   - Add another student then print the array and its new count.
    <br>- Finally, remove the firs student and print the array and its count.</h4>

    <pre><?php array_push($studentsArray, 'Jane Smith'); ?></pre>
    <pre><?php print_r($studentsArray);?></pre>
    <pre><?php echo count($studentsArray);?></pre>

    <pre><?php array_shift($studentsArray); ?></pre>
    <pre><?php print_r($studentsArray);?></pre>
    <pre><?php echo count($studentsArray);?></pre>

</body>
</html>